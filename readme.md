# ec2-windows-launchscript

## Overview

Update ec2 instance registry settings on launch using S3 bucket stored file

## Running PowerShell script on EC2 instance launch

1. Create an S3 bucket and store a json file with the desired registry settings, e.g. [registry-settings.json](./registry-settings.json):

2. Create IAM custom [policy](./iam-policy.json), replace bucket name with the name of your S3 bucket

3. Create new IAM role for EC2 instance and attach the policy

4. Launch new ec2 Windows Server 2016 instance and attach the role to the instance

5.  Substitute the `$regionName`, `$bucketName` and `$fileName` variables in the [script](./userdata.txt) and copy it to [UserData](https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/ec2-windows-user-data.html)

6. `<persist>` controls whether the script runs on instance re-start or on instance launch only
